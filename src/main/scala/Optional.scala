
import monocle.Optional
import monocle.law.OptionalLaws

object OptionalEx extends App{
  /*
  Gli Optional, come i Lens, consentono di fare operazioni di zoom all'interno di un Product (es. case class, Tuple,
  HList, Map) ma, a differenza di essi, in questo caso l'elemento sul quale ci si vuole focalizzare potrebbe anche
  non esistere. Un Optional si definisce come Optional[S,A] in cui S identifica il Product e A l'elemento opzionale
  all'interno di S.
  */

  /*
  La definizione di un Optional[List[Int],Int] che, partendo dalla lista List[Int] effettui un'operazione di zoom
  verso la sua potenziale testa, richiede la definizione di due funzioni:
    - getOption: List[Int] => Option[Int]
    - replace: Int => List[Int] => List[Int]
  */
  val head = Optional[List[Int], Int] {
      case Nil => None
      case x :: xs => Some(x)
      } { a => {
       case Nil => Nil
       case x :: xs => a :: xs
     }
  }


  /*
  Esempi di utilizzo dei metodi utili alla gestione degli Optional:
   - nonEmpty: per verificare il match dell'Optional ottenuto
   - getOrModify: per restituire l'oggetto target, se corrisponde, o il valore di partenza
   - replace: per sostituire il valore target
   - modify: per modificare il valore target
   - modifyOption: per conoscere l'esito, success o failure, della modifica del target
  */
  val xs = List(1, 2, 3)
  val ys = List.empty[Int]

  println(head.nonEmpty(xs))            // true
  println(head.nonEmpty(ys))            // false

  println(head.getOrModify(xs))         // Right(1)
  println(head.getOrModify(ys))         // Left(List())

  println(head.replace(5)(xs))          // List(5,2,3)
  println(head.replace(5)(ys))          // List()

  println(head.modify(_ + 1)(xs))       // List(2,2,3)
  println(head.modify(_ + 1)(ys))       // List()

  println(head.modifyOption(_ + 1)(xs)) // Some(List(2, 2, 3))
  println(head.modifyOption(_ + 1)(ys)) // None


  /*
  Controllo della validità di un Optional:
    - getOptionSet: verifica che l'applicazione del metodo getOrModify ad un valore A partendo da S permetta,
                    facendo un'operazione di replace, di ottenere come risultato un oggetto identico a quello
                    di partenza
    - setGetOption: verifica che, in seguito all'applicazione della funzione replace ad un valore, sia sempre possibile
                    tornare indietro mediante l'operazione getOption
   */
  class OptionalLaws[S, A](optional: Optional[S, A]) {
    def getOptionSet(s: S): Boolean =
      optional.getOrModify(s).fold(identity, optional.replace(_)(s)) == s

    def setGetOption(s: S, a: A): Boolean =
      optional.getOption(optional.replace(a)(s)) == optional.getOption(s).map(_ => a)
  }

  val optionalLaws = new OptionalLaws[List[Int], Int](head)
  println(optionalLaws.getOptionSet(List(1,2,3)))       // true
  println(optionalLaws.setGetOption(List(1,2,3),1))     // true

}
