//https://www.optics.dev/Monocle/docs/optics/iso

import monocle.Lens
import monocle.macros.{GenLens, Lenses}
import monocle.macros.syntax.lens._

case class Address(streetNumber: Int, streetName: String)
case class PersonWithAddress(name: String, age: Int, address: Address)
case class IdentityCard(number:Int, person: PersonWithAddress)

object LensEx extends App{

  /*
  Un Lens permette di fare un'operazione di zoom all'interno di un oggetto ed è definito da due parametri S e A tali
  che in Lens[S,A] vi sia S che rappresenta il prodotto e A che rappresenta l'elemento da visualizzare al suo interno.
  */

  /*
  Definizione di un Lens per fare zoom sul campo streetNumber all'interno di Address.
  */
  val getStreetNumber = Lens[Address,Int](_.streetNumber)(n => a => a.copy(streetNumber = n))
  val getStreetNumber2 = GenLens[Address](_.streetNumber)


  /*
  Definizione delle funzioni get e replace per l'utilizzo del Lens
    - get: Address => Int
    - replace: Int => Address => Address
  Per modificare il target di un Lens è possibile utilizzare:
    - replace: accetta il valore di tipo A da usare per la modifica
    - modify: accetta una funzione A => A che verrà applicata al valore da modificare
  */
  var address = Address(10, "High Street")
  println(getStreetNumber.get(address))           //10
  println(getStreetNumber.replace(7)(address))    //Address(7,High Street)
  println(getStreetNumber.modify(_ + 1)(address)) //Address(11,High Street)
  println(address)                                //Address(10,High Street)


  /*
  Esempio di utilizzo della funzione modifyF
  Definizione della funzione neighbors che restituisce la List[Int] dei numeri civici utilizzati per generare gli
  indirizzi dei "vicini di casa" rispetto al civico dell'address passato in input, estratto mediante il Lens
   */
  def neighbors(n: Int): List[Int] =
    if(n > 0) List(n - 1, n + 1)
    else List(n + 1)

  println(getStreetNumber.modifyF(neighbors)(address))    //List(Address(9,High Street), Address(11,High Street))
  println(getStreetNumber.modifyF(neighbors)(Address(0, "High Street")))           //List(Address(1,High Street))


  /*
  Esempio di composizione di Lens per effettuare operazioni di zoom più in profondità all'interno della struttura
  dati
  */
  val john = PersonWithAddress("John", 20, Address(10, "High Street"))
  val address2 = GenLens[PersonWithAddress](_.address)

  println(address2.andThen(getStreetNumber).get(john))          // 10
  println(address2.andThen(getStreetNumber).replace(2)(john))   // PersonWithAddress(John,20,Address(2,High Street))

  val tom = Person("Tom",25)
  val p = GenLens[Person](_.name).replace("Mike") compose GenLens[Person](_.age).replace(21)

  println(p.apply(tom))     // Person(Mike,21)
  println(john.focus(_.name).replace("Mike").focus(_.age).modify(_ + 1)) // PersonWithAddress(Mike,21,Address(10,High Street))

  /*
  Esempio di utilizzo di GenLens per la generazione di Lens a diversi livelli di profondità
   */
  val ic = IdentityCard(13, john)
  var myLens = GenLens[IdentityCard](_.person.address.streetName)

  println(myLens.replace("New Street")(ic)) // IdentityCard(13,PersonWithAddress(John,20,Address(10,New Street)))


  /*
  Esempio di utilizzo della macro @Lenses per la generazione di Lens per tutti i campi di una case class
  */
  @Lenses("_")
  case class MyPoint(x: Int, y: Int)
  val point = MyPoint(5, 3)

  println(MyPoint._x.get(point))          // 5
  println(MyPoint._y.get(point))          // 3
  println(MyPoint._x.replace(4)(point))  // MyPoint(4,3)
  println(MyPoint._y.replace(20)(point)) // MyPoint(5,20)


  /*
  Controllo della validità di un Lens:
    - getReplace: facendo la get di un valore A da S e applicando successivamente la replace per realizzare l'operazione
                  inversa, il risultato dovrà essere un oggetto identico a quello di partenza
    - replaceGet: dopo la sostituzione di un valore deve sempre essere possibile applicare l'operazione di get
                  al contrario per ottenere il valore di partenza
  */
  def getReplace[S, A](l: Lens[S, A], s: S): Boolean =
    l.replace(l.get(s))(s) == s

  def replaceGet[S, A](l: Lens[S, A], s: S, a: A): Boolean =
    l.get(l.replace(a)(s)) == a

  println(getReplace(myLens, ic))                 // true
  println(replaceGet(myLens, ic, "New Street"))   // true

}
