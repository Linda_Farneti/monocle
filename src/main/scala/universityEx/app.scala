package universityEx

import monocle.{Focus, Traversal}

object app extends App{

  var school = School("Hogwarts", Map(
    "Gryffindor" -> House("Griffindor", List(
      Student("Harry"  , "Potter", 5),
      Student("Ron", "Weasley", 10),
      Student("Hermione"  , "Granger", 15),
    )),
    "Slytherin" -> House("Slytherin", List(
      Student("Draco", "Malfoy", 20),
      Student("Lucius","Malfoy",0)
    )),
    "Ravenclaw" -> House("Ravenclaw", List(
      Student("Marcus", "Belby", 20),
      Student("Chang", "Cho", 20)
    ))
  ))

  /*
  Rimozione dell'intera casa "Ravenclaw"
  */
  val houses = Focus[School](_.houses)
  school = houses.at("Ravenclaw").replace(None)(school)
  println(school)

  // School(Hogwarts,Map(
  //        Gryffindor -> House(Griffindor,
  //                      List(Student(Harry,Potter,5), Student(Ron,Weasley,10), Student(Hermione,Granger,15))),
  //        Slytherin -> House(Slytherin,
  //                      List(Student(Draco,Malfoy,20), Student(Lucius,Malfoy,0)))))


  /*
  Aggiunta della casa "Hufflepuff" e dei relativi studenti
  */
  val hufflepuff = House("Hufflepuff", List(
    Student("Cedric", "Diggory", 15),
    Student("Ernie" , "Macmillan", 15)
  ))

  school = houses.at("Somehouse").replace(Some(hufflepuff))(school)
  println(school)

  // School(Hogwarts,Map(
  //      Gryffindor -> House(Griffindor,
  //                    List(Student(Harry,Potter,5), Student(Ron,Weasley,10), Student(Hermione,Granger,15))),
  //      Slytherin -> House(Slytherin,
  //                   List(Student(Draco,Malfoy,20), Student(Lucius,Malfoy,0))),
  //      Somehouse -> House(Hufflepuff,
  //                   List(Student(Cedric,Diggory,15), Student(Ernie,Macmillan,15))),
  // ))



  /*
  Modifica dei punteggi assegnati agli studenti
  */
  val students = Focus[House](_.student)
  val points = Focus[Student](_.points)
  val allStudents: Traversal[School, Student] = houses.each.andThen(students).each

  school = allStudents.andThen(points).modify(_ + 2)(school)
  println(school)

  // School(Hogwarts,Map(
  //            Gryffindor -> House(Griffindor,
  //                          List(Student(Harry,Potter,7), Student(Ron,Weasley,12), Student(Hermione,Granger,17))),
  //            Slytherin -> House(Slytherin,
  //                          List(Student(Draco,Malfoy,22), Student(Lucius,Malfoy,2))),
  //            Somehouse -> House(Hufflepuff,
  //                          List(Student(Cedric,Diggory,17), Student(Ernie,Macmillan,17)))
  //  ))

}
