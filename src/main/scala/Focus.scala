import monocle.syntax.all._

case class UserF(name: String, address: AddressF)
case class AddressF(streetNumber: Int, streetName: String)

object Focus extends App {
  /*
  Focus consente di definire un path all’interno di un oggetto immutabile allo scopo di permetterne sostituzioni
  e modifiche.
  */
  val user = UserF("Anna", AddressF(12, "high street"))

  user.focus(_.name).replace("Bob")                           // User(Bob,Address(12,high street))
  user.focus(_.address.streetName).modify(_.toUpperCase)      // User(Anna,Address(12,HIGH STREET))
  user.focus(_.address.streetNumber).get                      // 12

  /*
  Esempio di update di uno specifico campo presente all'interno di una case class
  */
  val anna = UserF("Anna", AddressF(12, "high street"))

  anna.focus(_.name).replace("Bob")                           // User = User(Bob,Address(12,high street))
  anna.focus(_.address.streetNumber).modify(_ + 1)            // User(Anna,Address(13,high street))

}
