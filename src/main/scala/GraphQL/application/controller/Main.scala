package GraphQL.application.controller

import GraphQL.application.codecutils.JsonConvert
import GraphQL.application.view.QueryPanel
import GraphQL.db.User


object Main extends App {

  import GraphQL.application.codecutils.ImplicitCodecs._

  new Controller().start()
}
