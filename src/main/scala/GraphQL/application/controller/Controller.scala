package GraphQL.application.controller

import GraphQL.application.model.{GqlObj, GqlParser, GqlResolver}
import GraphQL.application.view.QueryPanel

class Controller {

  private var gui= new QueryPanel()


  def runQuery(query:String): Unit ={
    println(query)

    try{
      // Conversione della stringa in input in un oggetto Gql rappresentante la query da risolvere
      val gqlObj = GqlParser.fromString(query)
      println(gqlObj)

      // Risoluzione della query
      val result = GqlResolver.resolve(gqlObj.asInstanceOf[GqlObj])
      println(result)

      // Stampa del risultato finale
      var s = "{"
      result.filter(x => x.isDefined()).foreach(x => s = s + x.print(None))
      s = s + "\n}"
      this.gui.setResult(s)
    } catch {
      case e => {
        this.gui.setError("Errore query: " + e)
      }
    }
  }

  def start(): Unit ={
    this.gui.buildInterface()
    this.gui.setUpController(this)
  }

}
