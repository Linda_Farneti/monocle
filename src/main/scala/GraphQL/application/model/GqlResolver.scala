package GraphQL.application.model
import GraphQL.db.{CommentNode, Database, GraphNode, PostNode, User, UserNode}
import monocle.Optional

object GqlResolver {

  def resolve(gqlObj:GqlObj):List[GraphNode] = {

    /*
    1 - Parsing degli elementi del db in collections
     */
    val users = Database.getUsers()
    val posts = Database.getPosts()
    val comments = Database.getComments()


    val userNodes = users.map(u => UserNode(u.id,None, None, None, None, None, None))
    val postNodes = posts.map(p => PostNode(p.id,None, None, p.userId, None, None, None, None))
    val commentNodes = comments.map(c => CommentNode(c.id,None, None, c.userId, None, c.postId, None, None, None, None))


    /*
     2 - Creazione dell'Optional necessario ad identificare il tipo degli oggetti Gql all'interno della query
     */
    val zoomGql = Optional[Gql, Gql] {
      case GqlNull => None
      case o:GqlObj => Some(o)
      case s:GqlStr => Some(s)
    }{
      _ => {
        case _ => GqlNull
      }
    }

    /*
    3 - Il primo zoom viene sempre fatto sul GqlObj "query", irrilevante per la determinazione del risultato, quindi
    per la valorizzazione dei campi verrà utilizzato come primo Obj il primo discendente di "query"
     */
    val queryObj = zoomGql.getOption(gqlObj).get.asInstanceOf[GqlObj]
    val obj1 = zoomGql.getOption(queryObj.nodes.head).get.asInstanceOf[GqlObj]

    /*
    L'Inspector, definito come implicito, unico al concetto di Reflection, permette che, dato il nome di un campo,
    in automatico si possano eseguire le operazioni di get e set su quello specifico campo
    */
    implicit def inspector(ref: AnyRef) = new {
      def getV(name: String): Any = ref.getClass.getMethods.find(_.getName == name).get.invoke(ref)
      def setV(name: String, value: Any): Unit = ref.getClass.getMethods.find(_.getName == name + "_$eq").get.invoke(ref, value.asInstanceOf[AnyRef])
    }

    /*
    Metodo che permette di filtrare gli user presenti a db in base alla condizione inserita nella query
     */
    def filterUsers(users:List[User])(userNodes:List[UserNode])(cond:Option[Condition]) = cond match {
      case Some(c) =>  filterCollection(userNodes)(uNode =>
        users.filter(_.getV(c.parameter).toString == c.value)
          .map(y => y.id)
          .contains(uNode._id)
      )
      case _ => userNodes
    }

    /*
    Metodo che permette di filtrare la Collection in base ad una condizione
     */
    def filterCollection[A](items:List[A])(p:(A) => Boolean):List[A] = {
      items.filter(p)
    }

    try {
      /*
      4 - Costruzione del risultato
       */
      inspectGql(obj1,None)
    } catch {
      case e => throw new Exception(s"malformed query (${e})")
    }

    def inspectGql(gql: Gql, nodes:Option[List[GraphNode]]):Unit ={
      gql match {
        case gqlObj:GqlObj =>  gqlObj.parameter match {
          case "users" => gqlObj.nodes.foreach(x => inspectGql(x, Some(filterUsers(users)(userNodes)(gqlObj.cond))))
          case "user" => nodes match {
            case l:Some[List[GraphNode]] =>
              l.get.head match {
                case _:PostNode =>
                  l.get.asInstanceOf[List[PostNode]].foreach(x=> x.user = Some(filterCollection[UserNode](userNodes)(_._id == x._userId).head))
                  gqlObj.nodes.foreach(x => {
                    val gql = zoomGql.getOption(x).get
                    inspectGql(gql, Some(userNodes))
                  })
                case _:CommentNode =>
                  l.get.asInstanceOf[List[CommentNode]].foreach(x=> x.user = Some(filterCollection[UserNode](userNodes)(_._id == x._userId).head))
                  gqlObj.nodes.foreach(x => {
                    val gql = zoomGql.getOption(x).get
                    inspectGql(gql, Some(userNodes))
                  })
              }
            case None => gqlObj.nodes.foreach(x => inspectGql(x, Some(filterUsers(users)(userNodes)(gqlObj.cond))))
          }
          case "posts" => nodes match {
            case l:Some[List[GraphNode]] =>
              l.get.head match{
                case _:UserNode=>
                  l.get.asInstanceOf[List[UserNode]].foreach(x => x.posts = Some(filterCollection[PostNode](postNodes)(_._userId == x._id)))
                  gqlObj.nodes.foreach(x => {
                    val gql = zoomGql.getOption(x).get
                    inspectGql(gql, Some(postNodes))
                  })
              }
            case None => gqlObj.nodes.foreach(x => inspectGql(x, Some(postNodes)))
          }
          case "post" => nodes match {
            case l: Some[List[GraphNode]] =>
              l.get.head match {
                case _: UserNode =>
                  l.get.asInstanceOf[List[PostNode]].foreach(x => x.user = Some(filterCollection[UserNode](userNodes)(_._id == x._userId).head))
                  gqlObj.nodes.foreach(x => {
                    val gql = zoomGql.getOption(x).get
                    inspectGql(gql, Some(postNodes))
                  })
                case _: CommentNode =>
                  l.get.asInstanceOf[List[CommentNode]].foreach(x => x.post = Some(filterCollection[PostNode](postNodes)(_._id == x._postId).head))
                  gqlObj.nodes.foreach(x => {
                    val gql = zoomGql.getOption(x).get
                    inspectGql(gql, Some(postNodes))
                  })
              }
          }
          case "comments"=>
            nodes match{
              case l:Some[List[GraphNode]]=>
                l.get.head match{
                  case _:UserNode=>
                    l.get.asInstanceOf[List[UserNode]].foreach(x=>x.comments=Some(filterCollection[CommentNode](commentNodes)(_._userId == x._id)))
                    gqlObj.nodes.foreach(x => {
                      val gql = zoomGql.getOption(x).get
                      inspectGql(gql, Some(commentNodes))
                    })
                  case _:PostNode=>
                    l.get.asInstanceOf[List[PostNode]].foreach(x=>x.comments=Some(filterCollection[CommentNode](commentNodes)(_._postId == x._id)))
                    gqlObj.nodes.foreach(x => {
                      val gql = zoomGql.getOption(x).get
                      inspectGql(gql, Some(commentNodes))
                    })
                }
              case _ => gqlObj.nodes.foreach(x=>inspectGql(x,Some(commentNodes)))
            }
          case "comment" => nodes match {
            case l: Some[List[GraphNode]] =>
              l.get.head match {
                case _: UserNode =>
                  l.get.asInstanceOf[List[UserNode]].foreach(x => x.comments = Some(filterCollection[CommentNode](commentNodes)(_._userId == x._id)))
                  gqlObj.nodes.foreach(x => {
                    val gql = zoomGql.getOption(x).get
                    inspectGql(gql, Some(commentNodes))
                  })
                case _: PostNode =>
                  l.get.asInstanceOf[List[PostNode]].foreach(x => x.comments = Some(filterCollection[CommentNode](commentNodes)(_._postId == x._id)))
                  gqlObj.nodes.foreach(x => {
                    val gql = zoomGql.getOption(x).get
                    inspectGql(gql, Some(commentNodes))
                  })
              }
            case _ => gqlObj.nodes.foreach(x=>inspectGql(x,Some(commentNodes)))
          }
        }
        case gqlStr:GqlStr => nodes match {
          case l:Some[List[GraphNode]] =>
            l.get.head match {
              case _:UserNode =>
                l.get.asInstanceOf[List[UserNode]].foreach(x => {
                  val user = users.filter(u => u.id == x._id).head  //cerca tra tutti gli utenti presi dal documento JSON quello con id specificato in (_id) dello UserNode attuale
                  x.setV(gqlStr.s, Some(user.getV(gqlStr.s)))
                } )
              case _:PostNode =>
                l.get.asInstanceOf[List[PostNode]].foreach(x => {
                  val post = posts.filter(p => p.id == x._id).head
                  x.setV(gqlStr.s, Some(post.getV(gqlStr.s)))
                } )
              case _:CommentNode =>
                l.get.asInstanceOf[List[CommentNode]].foreach(x => {
                  val comment = comments.filter(p => p.id == x._id).head
                  x.setV(gqlStr.s, Some(comment.getV(gqlStr.s)))
                } )
              case _ =>
            }
        }
        case _ =>
      }
    }

    /*
    Assegnazione della Collection risultato sulla base del primo parametro indicato nella query
     */
    obj1.parameter match {
      case "users" | "user" => userNodes
      case "posts" | "post" => postNodes
      case "comments" | "comment" => commentNodes
      case _ => throw new Exception(s"Argomento query  '${obj1.parameter}' non valido")
    }
  }

}


