package GraphQL.application.model

import monocle.{Iso}

sealed trait Gql
case class GqlStr(s:String) extends Gql
case class GqlObj(var parameter:String, cond:Option[Condition], var nodes: List[Gql]) extends Gql {}
case object GqlNull extends Gql

case class Condition(parameter:String, value:String)


object GqlParser{

  private val placeholder:String = "[]"

  /*
  Conversione della stringa in input in una lista di token
   */
  private val isoL = Iso[String, List[String]](
    _.replaceAll(" ", "")
    .replace("{","_{_")
    .replace("(","_(_")
    .replace(":", "_:_")
    .replace(")", "_)_")
    .replace(",","_,_")
    .replace("}","_}_")
    .replace("\n","")
    .split("_")
    .filter(_ != "").toList)(_.map(x => "{"+x+"}").mkString(""))
  /*
  il metodo mkString permette, partendo da un insieme di stringhe, di accorparle in una stringa unica definendone il
  separatore, in questo caso è stato utilizzato per definire il metodo reverseGet necessario alla corretta definizione
  dell'Iso ma l'operazione non viene utilizzata
   */

  /*
  stringa -> lista di token -> GqlNode
   */
  def fromString(s:String) = {
    val tokens = isoL.get(s)
    var skip = false
    // Lista che associa, ad ogni elemento Gql della query, il rispettivo livello di nesting
    var m:List[(Int,Gql)] = List()
    var currentIdx = 0

    /*
    Prima di costruire l'oggetto GqlObj finale, è necessario sapere quali sono gli oggetti Gql con cui comporlo e a quale
    livello di indentazione si trovano all'interno dell'oggetto.
     */
    for(i <- 0 to tokens.length - 1){
      tokens(i) match {
        case "{" => currentIdx = currentIdx + 1
        case "," => ;
        case "}" => currentIdx = currentIdx - 1
        case "(" => skip = true   //trovo una condition, fino a che non si chiude ')' skippo i token
        case ")" => skip = false
        case _
        => if(!skip) {
          {
            if(tokens.length > (i + 1) && tokens(i + 1) == "{"){  //se il carattere successivo è '{' allora è un GqlObj
              m = m :+ (currentIdx, new GqlObj(tokens(i), None, List()))
            } else if(tokens.length > (i + 1) && tokens(i + 1) == "(") {  //se il carattere successivo è '(', allora ho una condition
              /*
              L'individuazione di una condition considera di default che siano presenti tutti e 5 i token (si accettano
              solo le condizioni di uguaglianza), nella definizione risulta quindi possibile recuperare il nome e il
              valore del parametro utilizzando i rispettivi indici
               */
              m = m :+ (currentIdx, new GqlObj(tokens(i), Some(new Condition(tokens(i + 2), tokens(i + 4))), List()))
            } else {
              m = m :+ (currentIdx, new GqlStr(tokens(i)))
            }
          }
        }
      }
    }
    this.buildGqlNode(m.tail, (m.head._1, m.head._2.asInstanceOf[GqlObj]), None)
  }

  def toString(o:Some[Gql]):String = this.toString(o, List(), this.placeholder)


  /*
  Creazione dell'oggetto Gql corrispondente alla stringa di partenza
  */
  private def toString(g:Option[Gql], l:List[Gql], query:String):String = (g,l) match {
    case (o:Some[GqlObj], List()) => o.get.cond match{
      case c:Some[Condition] =>{
        this.toString(None, o.get.nodes,
          this.replaceWithContent(query, s"${o.get.parameter} (${c.get.parameter} : ${c.get.value}){ ${this.placeholder} }"))
      }
      case None => this.toString(None, o.get.nodes, this.replaceWithContent(query, s"${o.get.parameter} { ${this.placeholder} }"))
    }
    case (None, l:List[Gql]) => l match{
      case (h::Nil) => h match {
        case g_str:GqlStr => this.toString(None, List(), this.replaceWithContent(query, s"${g_str.s}"))
        case o:GqlObj => this.toString(None, List(), this.replaceWithContent(query, s"${this.toString(Some(o))}"))
      }
      case (h::t) => h match {
        case g_str:GqlStr => this.toString(None, t, this.replaceWithContent(query, s"${g_str.s}, ${this.placeholder}"))
        case o:GqlObj => this.toString(None, t, this.replaceWithContent(query, s"${this.toString(Some(o))}, ${this.placeholder}"))
      }
      case _ => query
    }
  }


  private def buildGqlNode(tokens:List[(Int,Gql)], fNode:(Int, GqlObj), cNode:Option[(Int, GqlObj)]): Gql ={
    tokens match {
      case h::t => {
        cNode match {
          case None =>
            h._2 match {
              case s:GqlStr =>
                fNode._2.nodes = fNode._2.nodes :+ s; buildGqlNode(t, fNode, None)
              case o:GqlObj =>
                buildGqlNode(t, fNode, Some(h._1, o))
            }
          case go: Some[(Int,GqlObj)] => {
            if(h._1 == go.get._1){
              /*
               Si assume che all'interno della query in uno stesso livello di nesting vengano scritte prima tutte le stringhe
               (GqlStr) e poi tutti i GqlObj
               */
              fNode._2.nodes = fNode._2.nodes :+ cNode.get._2; buildGqlNode(t, fNode, Some(h._1,h._2.asInstanceOf[GqlObj]))
            } else { //se la testa della lista ha un indice differente da quello del current node, allora
              h._2 match {
                case s:GqlStr =>
                  cNode.get._2.nodes = cNode.get._2.nodes :+ s; buildGqlNode(t, fNode, cNode)
                case o:GqlObj =>
                  buildGqlNode(t, cNode.get, Some(h._1, o)); fNode._2.nodes = fNode._2.nodes :+ cNode.get._2; fNode._2
              }
            }
          }
        }
      }
      case _ => fNode._2.nodes = fNode._2.nodes :+ cNode.get._2; fNode._2
    }
  }


  private def replaceWithContent(q:String, content:String): String ={
    q.replace(this.placeholder, content)
  }
}
