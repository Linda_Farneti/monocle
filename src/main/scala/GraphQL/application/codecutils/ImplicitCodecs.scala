package GraphQL.application.codecutils

import GraphQL.db.{Comment, Post, User}

/*
Dichiarazione degli impliciti necessari alle operazioni di serializzazione e deserializzazione delle entità del db
 */

object ImplicitCodecs {
  implicit val userCodec : Codec[User] = new UserCodec
  implicit val userListCodec : Codec[List[User]] = new UserListCodec
  implicit val postCodec : Codec[Post] = new PostCodec
  implicit val postListCodec : Codec[List[Post]] = new PostListCodec
  implicit val commentCodec: Codec[Comment] = new CommentCodec
  implicit val commentListCodec: Codec[List[Comment]] = new CommentListCodec
}
