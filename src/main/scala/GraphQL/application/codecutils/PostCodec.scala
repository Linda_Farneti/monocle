package GraphQL.application.codecutils

import GraphQL.db.Post
import io.circe.syntax.EncoderOps
import io.circe.{Json, parser}

case class PostListCodec() extends Codec[List[Post]]{
  override def toJson(l: List[Post]): String = {
    l.asJson.toString()
  }

  override def fromJson(s: String): List[Post] = {
    parser.parse(s) match {
      case Right(obj) => obj.as[List[Post]].getOrElse(Json.Null).asInstanceOf[List[Post]]
    }
  }
}

/*
La conversione del json in un Post è possibile grazie all'utilizzo dell'apposita annotation Circe.
L'implementazione di queste case class è stata necessaria poichè Circe non permetteva di realizzare la conversione
utilizzando i generici
 */
case class PostCodec() extends Codec[Post] {

  override def toJson(o: Post): String = {
    o.asJson.toString()
  }
  override def fromJson(s: String): Post = {
    parser.parse(s) match {
      /*
      La restituzione del Right(obj) significa che il parser è riuscito a realizzare la conversione, quindi la stringa
      in input era un json valido. Il cast a Post permette, se valido, di restituire un Post con tutti i suoi metodi
      e proprietà
      */
      case Right(obj) => obj.as[Post].getOrElse(Json.Null).asInstanceOf[Post]
    }
  }
}
