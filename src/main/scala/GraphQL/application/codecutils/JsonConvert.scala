package GraphQL.application.codecutils

/*
L'utilizzo dell'implicito permette l'applicazione della giusta strategia in base al tipo di A
Es. Se A è di tipo User, il Codec che verrà utilizzato sarà UserCodec
 */

object JsonConvert {
  def serializeObject[A](o: A)(implicit codec: Codec[A]): String = {
    codec.toJson(o)
  }

  def deserializeObject[A](s: String)(implicit codec: Codec[A]): A = {
    codec.fromJson(s)
  }
}
