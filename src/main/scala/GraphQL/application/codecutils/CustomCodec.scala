package GraphQL.application.codecutils
/*
Interfaccia che definisce i metodi necessari alla serializzazione e deserializzazione degli oggetti del db (poichè
le entità memorizzate sono di vari tipi, questa interfaccia è stata estesa con implementazioni custom per gli
specifici oggetti
 */

trait Codec[A]{
  def toJson(o:A):String
  def fromJson(s:String):A
}

