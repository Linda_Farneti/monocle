package GraphQL.application.codecutils

import GraphQL.db.{Comment}
import io.circe.syntax.EncoderOps
import io.circe.{Json, parser}


case class CommentListCodec() extends Codec[List[Comment]] {

  override def toJson(l: List[Comment]): String = {
    l.asJson.toString()
  }

  override def fromJson(s: String): List[Comment] = {
    parser.parse(s) match {
      case Right(obj) => obj.as[List[Comment]].getOrElse(Json.Null).asInstanceOf[List[Comment]]
    }
  }
}

/*
La conversione del json in un Comment è possibile grazie all'utilizzo dell'apposita annotation Circe.
L'implementazione di queste case class è stata necessaria poichè Circe non permetteva di realizzare la conversione
utilizzando i generici
 */
case class CommentCodec() extends Codec[Comment] {

  override def toJson(o: Comment): String = {
    o.asJson.toString()
  }
  override def fromJson(s: String): Comment = {
    parser.parse(s) match {
      /*
      La restituzione del Right(obj) significa che il parser è riuscito a realizzare la conversione, quindi la stringa
      in input era un json valido. Il cast a Comment permette, se valido, di restituire un Comment con tutti i suoi metodi
      e proprietà
      */
      case Right(obj) => obj.as[Comment].getOrElse(Json.Null).asInstanceOf[Comment]
    }
  }

}