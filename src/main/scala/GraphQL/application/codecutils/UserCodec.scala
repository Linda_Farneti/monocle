package GraphQL.application.codecutils

import GraphQL.db.User
import io.circe.syntax.EncoderOps
import io.circe.{Json, parser}

case class UserListCodec() extends Codec[List[User]] {
  override def toJson(l: List[User]): String = {
    l.asJson.toString()
  }

  override def fromJson(s: String): List[User] = {
    parser.parse(s) match {
      case Right(obj) => obj.as[List[User]].getOrElse(Json.Null).asInstanceOf[List[User]]
    }
  }
}

/*
La conversione del json in uno User è possibile grazie all'utilizzo dell'apposita annotation Circe.
L'implementazione di queste case class è stata necessaria poichè Circe non permetteva di realizzare la conversione
utilizzando i generici
 */
case class UserCodec() extends Codec[User] {
  override def toJson(o: User): String = {
    o.asJson.toString()
  }
  override def fromJson(s: String): User = {
    parser.parse(s) match {
        /*
        La restituzione del Right(obj) significa che il parser è riuscito a realizzare la conversione, quindi la stringa
        in input era un json valido. Il cast a User permette, se valido, di restituire uno User con tutti i suoi metodi
        e proprietà
        */
      case Right(obj) => obj.as[User].getOrElse(Json.Null).asInstanceOf[User]
      // case _ => throw new Exception("Parsing non valido")
    }
  }
}
