package GraphQL.application.view

import GraphQL.application.controller.Controller

import java.awt.BorderLayout
import java.awt.Color
import java.awt.Dimension
import java.awt.event.{ActionEvent, ActionListener, KeyEvent, KeyListener}
import javax.swing.{BorderFactory, JButton, JFileChooser, JFrame, JLabel, JPanel, JScrollPane, JTextArea, JTextField, ScrollPaneConstants, WindowConstants}
import javax.swing.border.TitledBorder


class QueryPanel {

  private val frame = new JFrame("Select Folder")
  private val centralPanel = new JPanel()
  private val gqlPanel = new JPanel()
  private val resultPanel = new JPanel()
  private val excludedPanel = new JPanel()
  private val controlPanel = new JPanel()
  private val bottomPanel = new JPanel()
  private val field = new JTextField(30)
  private val area = new JTextArea(4,60)
  private val gqlArea = new JTextArea(20,25)
  private val resultArea = new JTextArea(20,25)

  private val runQuery = new JButton("Run")
  private var controller:Controller = null

  def buildInterface() {
    this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    this.frame.setSize(700,600)
    this.frame.setBackground(Color.white)


    setCentralPanel()
    this.frame.getContentPane().add(this.centralPanel, BorderLayout.CENTER)

    setBottomPanel()
    this.frame.getContentPane().add(this.bottomPanel, BorderLayout.SOUTH)

    this.addEventHandlers();
    this.frame.setVisible(true)
  }


  private def setUpperPanel() {
    this.field.setEditable(false)
    this.gqlArea.setEditable(true)
    this.resultArea.setEditable(false)
    this.area.setEditable(false)
  }

  private def setCentralPanel() {

    var border1 = new TitledBorder(BorderFactory.createLineBorder(Color.blue), "Gql Query")
    border1.setTitleJustification(TitledBorder.CENTER)
    border1.setTitlePosition(TitledBorder.TOP)
    var scroll = new JScrollPane(this.gqlArea)
    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
    this.gqlPanel.setPreferredSize(new Dimension(300,400))
    this.gqlPanel.setBorder(border1)
    this.gqlPanel.add(scroll)

    this.centralPanel.add(this.gqlPanel, BorderLayout.WEST)


    this.resultPanel.setPreferredSize(new Dimension(100,30))
    this.centralPanel.add(this.runQuery, BorderLayout.CENTER)
    this.centralPanel.add(this.resultPanel, BorderLayout.CENTER)

    var border2 = new TitledBorder(BorderFactory.createLineBorder(Color.blue), "Result")
    border2.setTitleJustification(TitledBorder.CENTER)
    border2.setTitlePosition(TitledBorder.TOP)
    var scroll2 = new JScrollPane(this.resultArea)
    scroll2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
    this.excludedPanel.setBorder(border2)
    this.excludedPanel.setPreferredSize(new Dimension(300,400))
    this.excludedPanel.add(scroll2)
    this.centralPanel.add(this.excludedPanel, BorderLayout.EAST)

    this.controlPanel.setPreferredSize(new Dimension(600,500))


    this.centralPanel.add(this.controlPanel, BorderLayout.SOUTH)


  }

  private def setBottomPanel() {
    var border2 = new TitledBorder(BorderFactory.createLineBorder(Color.blue), "Messages")
    border2.setTitleJustification(TitledBorder.LEFT)
    border2.setTitlePosition(TitledBorder.TOP)
    var scroll = new JScrollPane(this.area)
    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS)
    this.resultPanel.setPreferredSize(new Dimension(630,100))
    this.resultPanel.setBorder(border2)
    this.resultArea.setTabSize(1)
    this.resultPanel.add(scroll)

    this.bottomPanel.add(resultPanel)
  }

  private def addEventHandlers(): Unit ={
    this.runQuery.addActionListener(new ActionListener {
      override def actionPerformed(actionEvent: ActionEvent): Unit = {
        QueryPanel.this.controller.runQuery(QueryPanel.this.gqlArea.getText())
      }
    })
  }

  def setUpController(c:Controller){
    this.controller = c
  }

  def setResult(res:String): Unit ={
    this.resultArea.setText(res)
  }

  def setError(error:String):Unit ={
    this.area.setText(error)
  }
}
