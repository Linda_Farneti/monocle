package GraphQL.db

import GraphQL.application.Configuration
import GraphQL.application.codecutils.{Codec, CommentListCodec, JsonConvert, PostListCodec, UserListCodec}

/*
L'object Database viene utilizzato per rendere disponibili le Collection users, posts e comments utili al GqlResolver
per determinare il risultato della query
 */

object Database {
  implicit val userListCodec : Codec[List[User]] = new UserListCodec
  implicit val postListCodec : Codec[List[Post]] = new PostListCodec
  implicit val commentListCodec: Codec[List[Comment]] = new CommentListCodec

  private val users = JsonConvert.deserializeObject[List[User]](scala.io.Source.fromFile(Configuration.pathToDbFiles+"users.json").mkString)
  private val posts = JsonConvert.deserializeObject[List[Post]](scala.io.Source.fromFile(Configuration.pathToDbFiles+"posts.json").mkString)
  private val comments = JsonConvert.deserializeObject[List[Comment]](scala.io.Source.fromFile(Configuration.pathToDbFiles+"comments.json").mkString)

  def getUsers():List[User] = users
  def getPosts():List[Post] = posts
  def getComments():List[Comment] = comments

}
