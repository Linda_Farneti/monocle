package GraphQL.db

import io.circe.generic.JsonCodec
import java.time.Instant

// L'annotation di Circe @JsonCodec permette di definire per quali classi è possibile realizzare la conversione in Json
@JsonCodec case class User(id:Int, name: String, surname: String, age: Int)
@JsonCodec case class Post(id:Int, text: String, userId: Int, date: Instant)
@JsonCodec case class Comment(id:Int, text:String, userId:Int, postId:Int, date: Instant)

/*
Le classi dichiarate all'interno di questo file permettono la generazione dei nodi del grafo delle informazioni e
delle relative relazioni presenti tra di esse.
 */

trait GraphNode{
  def isDefined():Boolean
  def print(i:Option[String]):String
}

case class UserNode(_id:Int, var id: Option[Int], var name: Option[String], var surname: Option[String], var age: Option[Int],
                    var posts: Option[List[PostNode]], var comments: Option[List[CommentNode]]) extends GraphNode {
  override def print(i:Option[String]): String ={
    var t = "\t"
    if(i.isDefined) t = t + i.get
    var s = s"\n${t}{"
    if(id.isDefined) s = s + s"\n${t}\tid: ${id.get},"
    if(name.isDefined) s = s+ s"\n${t}\tname: ${name.get},"
    if(surname.isDefined) s = s + s"\n${t}\tsurname: ${surname.get},"
    if(age.isDefined) s = s + s"\n${t}\tage: ${age.get},"

    if(posts.isDefined){
      s = s + s"\n${t}\tposts: {"
      posts.get.foreach(x => {
        s = t + s + x.print(Some(t+"\t"))
      })
      s = s + s"\n${t}\t}"
    }
    if(comments.isDefined){
      s = s + s"\n${t}\tcomments: {"
      comments.get.foreach(x => {
        s = t + s + x.print(Some(t+"\t"))
      })
      s = s + s"\n${t}\t}"
    }
    s = s + s"\n${t}}"
    s
  }

  override def isDefined(): Boolean = {
    id.isDefined | name.isDefined | surname.isDefined | age.isDefined | posts.isDefined | comments.isDefined
  }
}


case class PostNode(_id:Int, var id: Option[Int], var text: Option[String], _userId:Int, var userId: Option[Int],
                    var user: Option[UserNode],var date: Option[Instant], var comments: Option[List[CommentNode]]) extends GraphNode {
  override def print(i:Option[String]): String ={
    var t = "\t"
    if(i.isDefined) t = t + i.get
    var s = s"\n${t}{"
    if(id.isDefined) s = s + s"\n${t}\tid: ${id.get},"
    if(text.isDefined) s = s+ s"\n${t}\ttext: ${text.get},"
    if(date.isDefined) s = s + s"\n${t}\tdate: ${date.get}"
    if(userId.isDefined) s = s + s"\n${t}\tuserId: ${userId.get}"

    if(user.isDefined){
      s = s + s"\n${t}\tuser:"
      s = t + s + user.get.print(Some(t))
    }
    if(comments.isDefined){
      s = s + s"\n${t}\tcomments: {"
      comments.get.foreach(x => {
        s = t + s + x.print(Some(t+"\t"))
      })
      s = s + s"\n${t}\t}"
    }
    s = s + s"\n${t}}"
    s
  }

  override def isDefined(): Boolean = {
    id.isDefined | text.isDefined | date.isDefined | user.isDefined | userId.isDefined | comments.isDefined
  }
}


case class CommentNode(_id:Int, var id: Option[Int], var text: Option[String], _userId:Int, var userId: Option[Int],
                       _postId:Int, var postId: Option[Int], var date: Option[Instant], var user: Option[UserNode],
                       var post:Option[PostNode]) extends GraphNode {
  override def print(i:Option[String]): String ={
    var t = "\t"
    if(i.isDefined) t = t + i.get
    var s = s"\n${t}{"
    if(id.isDefined) s = s + s"\n${t}\tid: ${id.get},"
    if(text.isDefined) s = s+ s"\n${t}\ttext: ${text.get},"
    if(date.isDefined) s = s + s"\n${t}\tdate: ${date.get}"
    if(userId.isDefined) s = s + s"\n${t}\tuserId: ${userId.get}"

    if(user.isDefined){
      s = s + s"\n${t}\tuser:"
      s = t + s + user.get.print(Some(t))
    }
    if(post.isDefined){
      s = s + s"\n${t}\tpost:"
      s = t + s + post.get.print(Some(t))
    }
    s = s + s"\n${t}}"
    s
  }

  override def isDefined(): Boolean = {
    id.isDefined | text.isDefined | date.isDefined | user.isDefined | userId.isDefined | post.isDefined | postId.isDefined
  }

}

