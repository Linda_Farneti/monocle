
import alleycats.std.all.alleycatsStdInstancesForMap
import cats.Applicative
import monocle.{Optional, Traversal}
import cats.implicits._

case class Point(id: String, x: Int, y: Int)

object TraversalEx extends App {
  /*
  Il Traversal rappresenta una generalizzazione di un Optional applicata a più target poichè consente di focalizzarsi,
  partendo da un tipo S, su valori di tipo A da 0 a n
  */

  /*
  Esempio di utilizzo di un Traversal per recuperare tutti gli elementi all'interno di un Container (List, Vector, Option)
   */
  val xs = List(1, 2, 3, 4, 5)
  val eachL = Traversal.fromTraverse[List, Int]

  println(eachL.replace(0)(xs)) // List(0, 0, 0, 0, 0)
  println(eachL.modify(_ + 1)(xs)) // List(2, 3, 4, 5, 6)

  println(eachL.getAll(xs)) //  List(1,2,3,4,5)
  println(eachL.headOption(xs)) //  Some(1)
  println(eachL.find(_ > 3)(xs)) //  Some(4)
  println(eachL.all(_ % 2 == 0)(xs)) //  false


  /*
  Esempio di generazione di un Traversal
  Monocle mette a disposizione appositi costruttori per un numero prefissato di target (attualmente da 2 a 6)
   */
  val points = Traversal.apply2[Point, Int](_.x, _.y)((x, y, p) => p.copy(x = x, y = y))

  println(points.replace(5)(Point("bottom-left", 0, 0))) //Point(bottom-left,5,5)


  /*
  Esempio di definizione di un Traversal custom mediante l'utilizzo del metodo modifyA.
  Definizione di un Traversal per oggetti di tipo Map che effettui il focus sugli elementi la cui chiave soddisfa
  uno specifico predicato.
  */
  def filterKey[K, V](predicate: K => Boolean): Traversal[Map[K, V], V] =
    new Traversal[Map[K, V], V] {
      def modifyA[F[_] : Applicative](f: V => F[V])(s: Map[K, V]): F[Map[K, V]] =
        s.map { case (k, v) =>
          k -> (
            if (predicate(k)) f(v)
            else v.pure[F]
            )
        }.sequence
    }

  val m = Map(1 -> "one", 2 -> "two", 3 -> "three", 4 -> "Four")
  val filterEven = filterKey[Int, String](_ % 2 == 0)

  println(filterEven.modify(_.toUpperCase)(m)) // Map(1 -> one, 2 -> TWO, 3 -> three, 4 -> FOUR)

  /*
  Controllo della validità di un Traversal:
    - modifyGetAll: verifica che sia sempre possibile modifica tutti gli elementi individuati dal Traversal stesso
    - composeModify
  */
  class TraversalLaws[S, A](traversal: Traversal[S, A]) {
    def modifyGetAll(t: Traversal[S, A], s: S, f: A => A): Boolean =
      traversal.getAll(t.modify(f)(s)) == t.getAll(s).map(f)

    def composeModify(t: Traversal[S, A], s: S, f: A => A, g: A => A): Boolean =
      traversal.modify(g)(t.modify(f)(s)) == t.modify(g compose f)(s)
  }

  val traversalLaws = new TraversalLaws[List[Int], Int](eachL)
  def f(i: Int): Int = i+1
  def g(i: Int): Int = i-1

  println(traversalLaws.modifyGetAll(eachL, List(1,2,3), f))          // true
  println(traversalLaws.composeModify(eachL, List(1,2,3), f, g))      // true

}
