import monocle.Monocle.doubleToInt
import monocle.Prism
import monocle.macros.{GenIso, GenPrism}


sealed trait Json
case object JNull extends Json
case class JStr(v: String) extends Json
case class JNum(v: Double) extends Json
case class JObj(v: Map[String, Json]) extends Json

object PrismEx extends App{

  /*
  Un Prism permette di selezionare parti di un oggetto (Coproduct) e viene definito mediante l'utilizzo di due parametri
  tali che in un Prism[S,A] vi sia S che rappresenta il Sum type e A che identifica la relativa parte del Sum che si
  vuole selezionare.
  */

  /*
  Definizione di un Prism per ottenere, fra gli elementi di tipo Json, solo quelli che sono stati generati mediante
  l'uso del costruttore JStr.
  La funzioni necessarie sono:
    - getOption: Json => Option[String]
    - reverseGet: String => Json
  */
  val jStr = Prism[Json, String]{
    case JStr(v) => Some(v)
    case _ => None
  }(JStr)

  println(jStr("Hello"))                      //JStr(Hello)
  println(jStr.getOption(JStr("Hello")))      //Some(Hello)
  println(jStr.getOption(JNum(3.2)))          // None


  /*
  Definizione di un Prism mediante pattern matching sul costruttore
  In questo caso si ricorre all'utilizzo del metodo fornito partial che accetta in input una PartialFunction e
  permette di utilizzare getOption per applicare costruttore e pattern matcher al Prism oppure, nel secondo esempio,
  di usare direttamente il Prism in posizione di pattern matching.
  */
  val jStr2 = Prism.partial[Json, String]{
    case JStr(v) => v
  }(JStr)

  println(jStr2("hello"))                  // JStr("hello")
  println(jStr2.getOption(JStr("Hello")))  // Some(Hello)
  println(jStr2.getOption(JNum(3.2)))      // None

  def isLongString(json: Json): Boolean = json match {
    case jStr2(v) => v.length > 10
    case _ => false
  }

  println(isLongString(JNum(5.2))) // false
  println(isLongString(JStr("Hello"))) // false
  println(isLongString(JStr("Hello Hello Hello Hello Hello")))  // true


  /*
  Esempio di update di un Json di tipo JStr
  */
  println(jStr.replace("Bar")(JStr("Hello")))       //JStr(Bar)
  println(jStr.modify(_.reverse)(JStr("Hello")))    //JStr(olleH)

  println(jStr.replace("Bar")(JNum(10)))            //JNum(10.0)
  println(jStr.modify(_.reverse)(JNum(10)))         //JNum(10.0)


  /*
  Esempio di utilizzo dei metodi modifyOption e replaceOption che permettono di conoscere l'esito delle operazioni di
  update (success o failure).
  */
  println(jStr.modifyOption(_.reverse)(JStr("Hello")))   //Some(JStr(olleH))
  println(jStr.modifyOption(_.reverse)(JNum(10)))        //None
  println(jStr.replaceOption("Hola")(JStr("Hello")))     //Some(JStr(Hola))
  println(jStr.replaceOption("Hola")(JNum(10)))          //None


  /*
  Esempi di composizione di Prism
  Defizione di un Prism che realizza la conversione da Json a Double e applica poi una seconda conversione da Double a
  Int
  */
  val jNum: Prism[Json, Double] = Prism.partial[Json, Double]{case JNum(v) => v}(JNum)
  val jInt: Prism[Json, Int] = jNum.andThen(doubleToInt)

  println(jInt(5))                              //JNum(5.0)
  println(jInt.getOption(JNum(5.0)))            //Some(5)
  println(jNum.getOption(JNum(5.2)))            //Some(5.2)
  println(jInt.getOption(JNum(5.2)))            //None
  println(jInt.getOption(JStr("Hello")))        //None


  /*
  Esempi di generazione di Prism mediante l'utilizzo della macro GenPrism
  */
  val rawJNum: Prism[Json, JNum] = GenPrism[Json, JNum]

  println(rawJNum.getOption(JNum(4.5)))       //Some(JNum(4.5))
  println(rawJNum.getOption(JStr("Hello")))   //None

  val jNum1: Prism[Json, Double] = GenPrism[Json, JNum].andThen(GenIso[JNum, Double])
  val jNull: Prism[Json, Unit] = GenPrism[Json, JNull.type].andThen(GenIso.unit[JNull.type])

  println(jNum1.getOption(JNum(4.1)))         //Some(4.1)
  println(jNull.getOption(JStr("ciao")))      //None


  /*
  Controllo della validità di un Prism: i metodi getOption e reverseGet devono permettere di realizzare conversioni
  inverse e per poter tornare allo stato di partenza
  */
  def partialRoundTripOneWay[S, A](p: Prism[S, A], s: S): Boolean =
    p.getOption(s) match {
      case None    => true // nothing to prove
      case Some(a) => p.reverseGet(a) == s
    }

  def partialRoundTripOtherWay[S, A](p: Prism[S, A], a: A): Boolean =
    p.getOption(p.reverseGet(a)) == Some(a)

  println(partialRoundTripOtherWay(jNum1, 0.5))           //true
  println(partialRoundTripOneWay(jNum1, JNum(5.0)))       //true
}
