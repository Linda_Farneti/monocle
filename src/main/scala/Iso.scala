import GraphQL.db.User
import monocle._
import monocle.macros.GenIso

case class Person(name: String, age: Int)
case class MyString(s: String)

object IsoEx extends App{
  /*
  Un Iso permette di convertire elementi di tipo S in elementi di tipo A, senza che si verifichi alcuna
  perdita di informazioni.
  Considerando la case class Person, si ha che Person equivale ad una tupla (String,Int) e, viceversa, una tupla
  (String,Int) identifica un Person, quindi è possibile definire un Iso tra Person e (String,Int) utilizzando
  due funzioni:
      - get: Person => (String,Int)
      - reverseGet: (String,Int) => Person
   */
  val personToTuple = Iso[Person, (String, Int)](p => (p.name, p.age)) (t => Person(t._1, t._2))
  val tupleToPerson = personToTuple.reverse

  println(personToTuple.get(Person("Zoe", 25)) )    // (Zoe,25)
  println(personToTuple.reverseGet(("Zoe", 25)))    // Person(Zoe,25)

  println(personToTuple(("Mike",45)))               // Person(Mike,45)
  println(tupleToPerson(Person("Mike",45)))         // (Mike,45)


  /*
  Applicazione del concetto di Iso alle Collection
  Definizione di un Iso tra List[A] e Vector[A] e del corrispondente reverse
   */
  def listToVector[A] = Iso[List[A], Vector[A]](_.toVector)(_.toList)
  def vectorToList[A] = listToVector[A].reverse

  println(listToVector(Vector(1, 2, 3)))   //List(1, 2, 3)
  println(vectorToList(List(1, 2, 3)))     //Vector(1, 2, 3)
  println(vectorToList.reverseGet(List(1,2,3)) == listToVector.get(List(1,2,3)))    // true


  /*
  Applicazione di un Iso per la trasformazione di dati
  Definizione di un Iso tra String e List[Char] allo scopo di trasformare potenzialmente tutte le funzioni
  List[Char] => List[Char] in funzioni String => String
  */
  val stringToList = Iso[String, List[Char]](_.toList)(_.mkString(""))

  println(stringToList.get("Hello"))                           // List(H,e,l,l,o)
  println(stringToList.reverseGet(List('h','e','l','l','o')))  // hello
  println(stringToList.modify(_.tail)("Hello"))                // ello


  /*
  Utilizzo di macro GenIso.apply per la generazione di Iso
  */
  var s = GenIso[MyString, String].get(MyString("Hello"))
  var r = GenIso[MyString, String].reverseGet("Hello")
  println(s)  // Hello
  println(r)  // MyString(Hello)


  /*
  Utilizzo di macro GenIso.fields per ottenere la sequenza di campi di un certo oggetto
  */
  case class UserP(var name: String, var surname: String, var age: Int)

  val user = UserP("Mario","Rossi", 52)
  val myIso = GenIso.fields[UserP]

  println(myIso.get(user))     // (Mario,Rossi,52)


  /*
  Controllo della validità di un Iso: l'operazione di get e quella di reverseGet devono essere inverse
  */
  def roundTripOneWay[S, A](i: Iso[S, A], s: S): Boolean =
    i.reverseGet(i.get(s)) == s

  def roundTripOtherWay[S, A](i: Iso[S, A], a: A): Boolean =
    i.get(i.reverseGet(a)) == a

  println(roundTripOneWay(personToTuple, Person("Zoe",25)))   // true
  println(roundTripOtherWay(personToTuple, ("Zoe",25)))       // true

}
